## Living Room

|No|Name|x_coordinate|y_coordinate|tag_type|
|---|---|---|---|---|
|1|SOFA 3 SEATER HARPER 200X85X90CM DARK GREY|466|285|-|
|2|BED SIDE TABLE MARIUS 40X30X59CM OAK|618|340|-|
|3|Coffee Table BENEDIKTE 120cm 1 drawer walnut/white|291|351|-|
|4|CONSOLE TABLE ELVIS 120X39X75CM SONOMA OAK|-|-|-
|5|FLOOR LAMP GUDMUND DIAMETER 39XH160CM WHITE|411|182|-|
|6|DINING SETS APETIDO 120X80X75 WHITE|1032|371|-|


## Ruang Tidur Utama

|No|Name|x_coordinate|y_coordinate|tag_type|
|---|---|---|---|---|
|1|Lemari WARDROBE SLIDING DOORS KEN 150X59X198CM WALNUT|682|233|-|
|2|Chair DC BEGIVEN 46x46x82 WHITE|878|399|-|
|3|Lamp TABLE LAMP WATERSTONE GRAY|161|338|-|
|4|Ranging S2 DENISE BED KING|380|271|-|
|5|Night Stand S2 TAYLOR NIGHT STAND|209|403|-|
|6|Tv Sharp Full-HD Easy Smart 3.0|1059|167|-|
|7|AC LG DUALCOOL with Watt Control-Smart 1PK|1019|32|-|
|8|Bed MATTRESS BASIC 100X200|559|337|-|


## Ruang Tidur 1

|No|Name|x_coordinate|y_coordinate|tag_type|
|---|---|---|---|---|
|1|Lemari WARDROBE TERNING 2 DOOR WITH LOCK 67X50X180CM WHITE|340|250|-|
|2|Chair DC TOMMERUP CHROME/BLACK|496|319|-|
|3|Meja Belajar OFFICE DESK WATSON 120X60X75 WALNUT|633|311|-|

## Ruang Tidur 2

|No|Name|x_coordinate|y_coordinate|tag_type|
|---|---|---|---|---|
|1|POUFFE HOLEBY WITH STORAGE 40X40CM CREAM|663|355|-|
|2|ALU BLIND 100X160CM SILVER |541|183|-|
|3|WARDROBE 2 DOORS PRICE STAR 96X176X50CM WHITE|721|246|-|
|4|MATTRESS SOVE CASA SET POCKET LATEX 180x200x32cm|488|305|-|

## Dapur

|No|Name|x_coordinate|y_coordinate|tag_type|
|---|---|---|---|---|
|1|Cookerhood DIVA ISOLA - IX 9330|596|129|-|
|2|Oven BRAVO - BO 6633|620|375|-|
|3|Sink Bak cuci tanam, 1 bak+tpt pengering, baja tahan karat|945|269|-|
|4|Panci Panci dengan penutup, kaca/baja tahan karat|566|251|-|
|5|Kompor, DINING EXECUTIVE STOVE w-999 |612|271|-|
|6|Dispenser Water Dispenser Top Loading - DD 07 W|477|304|-|

## Toilet

|No|Name|x_coordinate|y_coordinate|tag_type|
|---|---|---|---|---|
|1|LW565 LAVATORY|333|278|-|
|2|CW826J/SW826JP CLOSET|738|370|-|
|3|TX403SB Shower Spray With Stop Valve (White)|-|-|-|
|4|KRAN, TX303BEBR |291|261|-|
|5|Paper Holder TX703AE |931|316|-|
|6|Shower THX19B|-|-|-|
|7|WALL RACK 2 SHELVES EDMUND 39X22X54CM METAL|128|197|-|
